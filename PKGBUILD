# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Based on the file created for Arch Linux by:
# Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Bob Fanger <bfanger(at)gmail>
# Filip <fila pruda com>, Det <nimetonmaili(at)gmail>

_linuxprefix=linux517-rt
_extramodules=extramodules-5.17-rt-MANJARO
_pkgname=r8168
pkgname=$_linuxprefix-$_pkgname
pkgver=8.049.02
pkgrel=4
pkgdesc="A kernel module for Realtek 8168 network cards"
url="http://www.realtek.com.tw"
license=("GPL")
arch=('x86_64')
depends=('glibc' "$_linuxprefix")
makedepends=("$_linuxprefix-headers")
provides=("$_pkgname=$pkgver")
replaces=("linux516-rt-$_pkgname")
groups=("$_linuxprefix-extramodules")
source=("https://github.com/mtorromeo/r8168/archive/refs/tags/$pkgver.tar.gz")
sha256sums=('2b12d932e976f8f74b8d9545744c04beb4676dd7bc1d032dde51347ed1d8be24')

install=$_pkgname.install

prepare() {
    cd "$_pkgname-$pkgver"
}

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version || true)"

    cd "$_pkgname-$pkgver"

    # avoid using the Makefile directly -- it doesn't understand
    # any kernel but the current.

    make -C /usr/lib/modules/$_kernver/build \
      M="$srcdir/$_pkgname-$pkgver/src" \
      EXTRA_CFLAGS="-DCONFIG_R8168_NAPI -DCONFIG_R8168_VLAN -DCONFIG_ASPM -DENABLE_S5WOL -DENABLE_EEE" \
      modules
}

package() {
    cd "$_pkgname-$pkgver/src"
    install -D -m644 $_pkgname.ko "$pkgdir/usr/lib/modules/$_extramodules/$_pkgname.ko"

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${_pkgname}.install"

    find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
}
